
#! /bin/sh
#rkhunter-check.sh - EARTH.SOFIBOX.COM
echo "=========================="
echo "Rkhunter is checking system..."
echo "Please wait..."
MYFILENAME="rkhunter-report.txt"
GREPRESULT="rkhunter-grep-result.txt"
FINALREPORT="final-rkhunter-report.txt"
/bin/echo "Rkhunter checked on `date`" > /tmp/$MYFILENAME
/usr/bin/rkhunter --versioncheck >> /tmp/$MYFILENAME
/usr/bin/rkhunter --update >> /tmp/$MYFILENAME
/usr/bin/rkhunter --propupd >> /tmp/$MYFILENAME
/usr/bin/rkhunter -c --sk 2>&1 >> /tmp/$MYFILENAME
/bin/echo "**************************************" >> /tmp/$MYFILENAME
/bin/echo "***********DONE SCANNING!*************" >> /tmp/$MYFILENAME

#Remove the system character output to be able to read by email without having to send as attachment
/bin/sed $'s/[^[:print:]\t]//g' /tmp/$MYFILENAME > /tmp/$FINALREPORT

#Check if the final report contains warning, do not use the option -q (quite) as it will not output to GREPRESULT
if (grep "Warning" /tmp/$FINALREPORT >> /tmp/$GREPRESULT) then
echo "WARNING FOUND"
/bin/echo "======= warning found! =============" >> /tmp/$GREPRESULT
#Append the GREPRESULT at the final report
/bin/cat /tmp/$GREPRESULT >> /tmp/$FINALREPORT
/bin/mail -s "[rkhunter] Warning FOUND @ EARTH.SOFIBOX.COM!" webmaster@sofibox.com < /tmp/$FINALREPORT
echo "=========================="
else

echo "NO WARNING FOUND"
/bin/echo "======= NO warning found  =============" >> /tmp/$MYFILENAME
/bin/mail -s "[rkhunter] NO Warning FOUND @ EARTH.SOFIBOX.COM!" webmaster@sofibox.com < /tmp/$FINALREPORT
echo "=========================="
fi

#REMOVE /tmp result

rm -rf /tmp/$MYFILENAME
rm -rf /tmp/$GREPRESULT
rm -rf /tmp/$FINALREPORT

echo "Rkhunter: Done checking system. Email is set to be sent to webmaster@sofibox.com"
echo "========================="
